from django.contrib import admin

# Register your models here.
from .models import Input_status, SubscribeModels
admin.site.register(Input_status)
admin.site.register(SubscribeModels)
