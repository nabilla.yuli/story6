from django import forms
from .models import SubscribeModels, Input_status
from django.core.validators import RegexValidator

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Wajib diisi yaa!',
    }
    title_attrs = {
        'type': 'content-text',
        'class': 'form-control',
        'placeholder':'What is your status today?'
    }
    status_in = forms.CharField(label='', max_length=300, widget=forms.TextInput(attrs=title_attrs))

class SubscribeForms(forms.Form):
    error_messages = {
        'required': 'Wajib diisi yaa!',
    }
    name_attrs={
        'id' : 'name',
        'class' : 'todo-form-input',
        'placeholder' : 'Your name',
    }

    email_attrs={
        'id' : 'email',
        'class' : 'todo-form-input',
        'placeholder' : 'Your email',
    }

    password_attrs={
        'id' : 'password',
        'class' : 'todo-form-input',
        'placeholder' : 'Password should be a combination of Alphabets and Numbers',
    }
    
    name = forms.CharField (label = "", required = True, widget=forms.TextInput(attrs = name_attrs))
    email = forms.EmailField (label = "", required = True, widget=forms.EmailInput(attrs = email_attrs))
    password = forms.CharField(label = "", validators=[RegexValidator('^(\w+\d+|\d+\w+)+$')], max_length=30, required=True,
                                widget=forms.PasswordInput(attrs = password_attrs))

  