from django.db import IntegrityError
from django.test import TestCase
from django.test import Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import unittest
import time

from .views import index, post_status, profile_page1, library, getJSON, SubscribeViews, check_email, post_subscriber
from .models import Input_status, SubscribeModels
from .forms import Status_Form, SubscribeForms

# Create your tests here.
class Lab6Test(TestCase):

    def test_Lab6_url(self):
        response = Client().get('/Lab6/')
        self.assertEqual(response.status_code, 200)

    def test_Lab6_using_index_func(self):
        found = resolve('/Lab6/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new status
        status_in = Input_status.objects.create(status_in = 'Hello')
        # Retrieving all available activity
        counting_all_status_in = Input_status.objects.all().count()
        self.assertEqual(counting_all_status_in, 1)

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status_in' : ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_in'][0],
            'This field is required.'
        )
 
    def test_Lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post_status/', {'status_in': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/Lab6/')
        html_response = response.content.decode('utf-8')
        self.assertIn(test, html_response)

    def test_Lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post_status/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertNotIn(test, html_response)

    def test_Lab9_getJSON_url_is_exist(self):
        response = Client().get('/getJSON?q=quilting/')
        self.assertEqual(response.status_code, 200)

    def test_Lab9_library_url_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)
    
    def test_Lab9_library_using_library_function(self):
        found = resolve('/library/')
        self.assertEqual(found.func, library)
    
    def test_Lab9_library_using_to_do_list_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'Library.html')

    def test_Lab10_subcribe_page_url_is_exist(self):
        response = Client().get('/SubscribeViews/')
        self.assertEqual(response.status_code, 200)
    
    def test_Lab10_subscribe_using_function(self):
        found = resolve('/SubscribeViews/')
        self.assertEqual(found.func, SubscribeViews)

    def test_Lab10_model_can_create_new_data(self):
        data = SubscribeModels.objects.create(name="nabilla", email="nabilla@gmail.com", password="yuli890")
        counting_data = SubscribeModels.objects.all().count()
        self.assertEqual(counting_data, 1)
    
    def test_Lab10_self_func_name(self):
        SubscribeModels.objects.create(name="nabilla", email="nabilla@gmail.com", password="yuli890")
        subscriber = SubscribeModels.objects.get(email='nabilla@gmail.com')
        self.assertEqual('nabilla@gmail.com', subscriber.email)

    def test_Lab10_email_must_unique(self):
        SubscribeModels.objects.create(email="nabilla@gmail.com")
        with self.assertRaises(IntegrityError):
            SubscribeModels.objects.create(email="nabilla@gmail.com")

    def test_Lab10_post_using_ajax(self):
        response = Client().post('/post_subscriber/', data={
            "name": "nabilla",
            "email": "nabilla@gmail.com",
            "password": "yuli890",
        })
        self.assertEqual(response.status_code, 200)

    def test_Lab10_check_email_view_return_200(self):
        response = Client().post('/check_email/', data={
            "email": "nabilla@gmail.com"
        })
        self.assertEqual(response.status_code, 200)
    
    def test_Lab10_email_already_exist_view_return_200(self):
        SubscribeModels.objects.create(name="nabilla",
                                      email="nabilla@gmail.com",
                                      password="yuli890")
        response = Client().post('/check_email/', data={
            "email": "nabilla@gmail.com"
        })
        self.assertEqual(response.json()['is_exists'], True)

    def test_Lab10_if_subform_is_blank(self):
        form = SubscribeForms(data={
            "name": "",
            "email": "",
            "password": "",
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'][0],
            'This field is required.',
        )

    def test_Lab11_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)

    def test_lab11_false_token(self):
        response = Client().post('/login', {'id_token': "testtt"}).json()
        self.assertEqual(response['status'], "1")

    def test_Lab11_right_token(self):
        response = Client.post('/login', {'id_token' :"608281685841-k2ub86p92rn3vmkmb13boe2ccfvll65d.apps.googleusercontent.com"}).json()
        self.assertEqual(response['status'], "0")

    def test_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code, 302)



#fungtional test
# class Lab6FungsionalTest(unittest.TestCase):
#     def setUp(self):
#         capabilities = {
#             'browserName': 'chrome',
#             'chromeOptions': {
#                 'useAutomationExtension': False,
#                 'forceDevToolsScreenshot': True,
#                 'args': ['--no-sandbox','disable-dev-shm-usage']
#             }
#         }
#         self.browser = webdriver.Chrome('./chromedriver', desired_capabilities=capabilities)
#         super(Lab6FungsionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(Lab6FungsionalTest, self).tearDown()
    
#     def test_can_input_status(self):
# 	    #opening the link we want to test
#         self.browser.get('http://127.0.0.1:8000/')
# 	    #find the form element
#         status_box = self.browser.find_element_by_id('id_status_in')
#         status_box.send_keys('Coba Coba')
#         status_box.submit()
#         self.assertIn( 'Coba', self.browser.page_source)

#     def test_layout(self):
#         self.browser.get('http://127.0.0.1:8000/')
#         tittle_box = self.browser.find_element_by_id('s-text')
#         self.assertIn( 'Hello', self.browser.page_source)

#     def test_tittle(self):
#         self.browser.get('http://127.0.0.1:8000/')
#         title = self.browser.title
#         self.assertIn('Status Ku', title)

#     def test_background_color(self):
#         self.browser.get('http://127.0.0.1:8000/')
#         background_color = self.browser.find_element_by_tag_name('body')
#         color_test = background_color.value_of_css_property('background-color')
#         self.assertEqual( 'rgba(255, 255, 255, 1)', color_test)

#     def test_background_image(self):
#         self.browser.get('http://127.0.0.1:8000/')
#         background_img = self.browser.find_element_by_class_name('page')
#         img_test = background_img.value_of_css_property('background-size')
#         self.assertEqual( '100%', img_test)

# if __name__ == '__main__': #
#     unittest.main(warnings='ignore') #
        



