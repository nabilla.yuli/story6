from django.db import models

# Create your models here.
class Input_status(models.Model):
    status_in = models.CharField(max_length=300)

class SubscribeModels(models.Model):
   name = models.CharField(max_length=32)
   email = models.EmailField(max_length = 32, unique=True)
   password = models.CharField(max_length = 32)

