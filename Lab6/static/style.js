$(document).ready(function(){
    $('.item').click(function () {
        $(this).next().slideToggle(100);
        $('p').not($(this).next()).slideUp('fast');
    });
    
    $('#theme').click(function(){
        $(".jumbotron").toggleClass('jumbotron1');
        $(".jumbotron img").toggleClass('jumbotron1 img');
        $(".page").toggleClass('page1');
        $(".forms").toggleClass('forms1');
        $(".lal").toggleClass('lal1');
        $(".accordion").toggleClass('accordion2');
    });

    /* Lab 9 */
    $.ajax({
        method: "GET",
        url: "getJSON?q=quilting",
        success: function (result){
            var books = result.data;
            for (var i = 0; i < books.length; i++){
                var title = books[i].title;
                var authors = books[i].authors;
                var description = books[i].description;
                var published = books[i].published;
                var id = books[i].id;
                var button = '<img id="' + id + '" width="30" height="30" src="/static/star.png" onclick="addFavorite(id)">';
                
                var html = '<tr >' + 
                            '<td>' + '<img src= ' + '"' + books[i].image + '">' + '</td>' + 
                            '<td>' + title + '</td>' + 
                            '<td>' + authors + '</td>' + 
                            '<td>' + description + '</td>' + 
                            '<td>' + published + '</td>' + 
                            '<td>' + button + '</td>' + 
                            '</tr>';
                $('#buku').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ada")
        }
    });

    /* Lab 10 Chalennge */
    $.ajax({
        method: "GET",
        url: "list_subscriber",
        success: function (subs_list){
            for (var i = 0; i < subs_list.length; i++){
                var name = subs_list[i]['name'];
                var email = subs_list[i]['email'];
                var button = '<button class="btn btn-danger" id=' + email + ' onclick=unSubscribe(this)>' + 'unsubscribe' + '</button>';
                var html = '<tr>' + 
                            '<td>' + name + '</td>' + 
                            '<td>' + email + '</td>' + 
                            '<td>' + button + '</td>' +
                            '</tr>';
                $('#daftar').append(html);
            }
        },
        error: function(error){
            alert("Data subscriber tidak ada")
        }
    });
});

/* Lab 10 Challenge */
function unSubscribe(t){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email = t.id;

    $.ajax({
        method:"POST",
        url: "delete",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {email: email},
        success: function(res) {
            console.log('success');
            $('#daftar').replaceWith("<tbody id='daftar'></tbody>");
            var data = res.now;
            for (var i = 0; i < data.length; i++) {
                var name = data[i]['name'];
                var email = data[i]['email'];
                var button = '<button class="btn btn-danger" id=' + email + ' onclick=unSubscribe(this)>' + 'unsubscribe' + '</button>';
                var html = '<tr>' + 
                            '<td>' + name + '</td>'+ 
                            '<td>' + email + '</td>' + 
                            '<td>' + button + '</td>' +
                            '</tr>';
                $('#daftar').append(html)
            }

        },
        error: function(error){
            alert("Data tidak ada");
        }
    });
}

var query = "quilting";
$(".submit").on("click",function() {
    query = this.id;
    $.ajax({
        method: "GET",
        url: "getJSON?q=" + query,
        success: function (result){
            $('td').remove();
            var books = result.data;
            for (var i = 0; i < books.length; i++){
                var title = books[i].title;
                var authors = books[i].authors;
                var description = books[i].description;
                var published = books[i].published;
                var id = books[i].id;
                var button = '<img id="' + id + '" width="30" height="30" src="/static/star.png" onclick="addFavorite(id)">';
                
                var html = '<tr >' + 
                            '<td>' + '<img src= ' + '"' + books[i].image + '">' + '</td>' + 
                            '<td>' + title + '</td>' + 
                            '<td>' + authors + '</td>' + 
                            '<td>' + description + '</td>' + 
                            '<td>' + published + '</td>' + 
                            '<td>' + button + '</td>' + 
                            '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ada")
        }
    });
});

function addFavorite(id) {
    $.ajax({
        method:"GET",
        url: "getJSON?q=" + query,
        success: function(result) {
            var data = result.data;
            count = document.getElementById('Fav').innerHTML;
            countFav = parseInt(count);
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/star.png")) {
                        img.src = "/static/favstar.png";
                        countFav++;
                    } else {
                        img.src = "/static/star.png";
                        countFav--;
                    }
                }
            }
            $('#Fav').replaceWith('<span id="Fav">' + countFav + '</span>');
        },
        error: function(error){
            alert("Buku tidak ada");
        }
    });
}

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

$
/*Lab 10 */
$(function () {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email_available;
    var timer = 0;

    $('#email').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(emailFormat, 1000);
        toggleButton();
    });

    $('#name').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#password').keydown(function () {
        clearTimeout(timer);
        timer = setTimeout(toggleButton, 1000);
    });

    $('#form').on('submit', function (event) {
        event.preventDefault();
        console.log("Form Submitted!");
        post_data();
    });

    function post_data() {
        $.ajax({
            method: 'POST',
            url: "post_subscriber",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                name: $('#name').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                if (response.is_success) {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#subsbtn').prop('disabled', true);
                    $('.confirmation p').replaceWith("<p class='yes'> Thank You for being my subscriber:) </p>");
                    console.log("Success!");
                } else {
                    $('.confirmation p').replaceWith("<p class='no'> Something wrong!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })
    }

    function emailFormat() {
        var reg = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = reg.test($('#email').val());
        if (is_valid) {
            checkEmail();
        } else {
            $('.confirmation p').replaceWith("<p class='fail'>Please enter a valid email</p>");
        }
    }

    function checkEmail() {
        $.ajax({
            method: 'POST',
            url: "check_email",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    email_available = false;
                    $('.confirmation p').replaceWith("<p class='no'>Please enter another email! This email is already been used.</p>");
                } else {
                    email_available = true;
                    toggleButton();
                }
            },
            error: function () {
                alert("Error, cannot validate email!")
            }
        })
    }

    function toggleButton() {
        var name = $('#name').val();
        var email = $('#email').val();
        var password = $('#password').val();
    
        if (name.length !== 0 && email_available && password.length !== 0) {
            $('.confirmation p').replaceWith("<p></p>");
            $('#subsbtn').prop('disabled', false);
        } 
        else if (name.length === 0 && email.length == 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Name and email cannot be empty</p>");
        }
        else if (name.length === 0 && password.length === 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Name and password cannot be empty</p>");
        } 
        else if (password.length === 0 && email.length === 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Email and password cannot be empty</p>");
        } 
        else if (name.length === 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Name cannot be empty</p>");
        }
        else if (email.length === 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Email cannot be empty</p>");
        } 
        else if (password.length === 0) {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Password cannot be empty</p>");
        } 
        else {
            $('#subsbtn').prop('disabled', true);
            $('.confirmation p').replaceWith("<p>Please enter a valid email format!</p>");
        }
    }
});

/* Lab 11 */
$(document).ready(function () {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
});

function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    sendToken(id_token);
    console.log("Log In");
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        console.log('User signed out.');
    });
}

function sendToken(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var html;

    $.ajax({
        method: "POST",
        url: "/login/",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token},
        success: function (result) {
            console.log("Successfully send token");
            if (result.status === "0") {
                html = "<h4 class='success'>Logged In</h4>";
                window.location.replace(result.url);
            } else {
                html = "<h4 class='fail'>Something error, please report</h4>"
            }
            $("h4").replaceWith(html)
        },
        error: function () {
            alert("Something error, please report")
        }
    })
}




