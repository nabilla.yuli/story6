from django.conf.urls import url
from .views import index, post_status, profile_page1, library, getJSON, SubscribeViews, post_subscriber, check_email, mySubscriber, list_subscriber, delete, login, logout

urlpatterns = [
    url('index', index),
    url('post_status', post_status, name='post_status'),
    url('profile_page1', profile_page1, name='profile_page'),
    url('library', library, name='library'),
    url('getJSON', getJSON, name='getJSON'),
    url('SubscribeViews', SubscribeViews, name='SubscribeViews'),
    url('check_email', check_email, name='check_email'),
    url('post_subscriber', post_subscriber, name='post_subscriber'),
    url('list_subscriber', list_subscriber, name='list_subscriber'),
    url('mySubscriber', mySubscriber, name='mySubscriber'),
    url('delete', delete, name='delete'),
    url('login', login, name='login'),
    url('logout', logout, name='logout'),
    url('', index, name='index')
]