from django.http import JsonResponse
from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt

from .forms import Status_Form, SubscribeForms
from .models import Input_status, SubscribeModels
import requests
import json


# Create your views here.
response = {'author' : 'Nabilla Yuli Shafira'}

def index(request):
    result = Input_status.objects.all()
    response['res'] = result
    response['form'] = Status_Form
    return render(request, 'landing_page.html', response)

def post_status(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['res'] = request.POST['status_in']
        result = Input_status(status_in=response['res'])
        result.save()
        return HttpResponseRedirect('/Lab6/')
    else:
        return HttpResponseRedirect('/Lab6/')

def profile_page1(request):
    return render(request, 'profile_page1.html')

def getJSON(request):
    query = request.GET['q']
    url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    jsonSrc = requests.get(url).json()
    result = jsonSrc['items']
    newResult = [] 
    for items in result:
        data = items['id']
        newDict = {"image": items['volumeInfo']['imageLinks']['thumbnail'], 
                    "title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "description": items['volumeInfo']['description'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id']
        }
        newResult.append(newDict)
    return JsonResponse({'data': newResult})

def library(request):
    return render(request, 'Library.html')

def SubscribeViews(request):
    form = SubscribeForms(request.POST or None)
    response['form'] =  form
    html = 'subscribe.html'
    return render(request, html, response)

def post_subscriber(request):
    form = SubscribeForms(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']
        SubscribeModels.objects.create(name=name, email=email, password=password)
        return JsonResponse({'is_success': True})

@csrf_exempt
def check_email(request):
    if request.method == "POST":
        email = request.POST['email']
        checked = SubscribeModels.objects.filter(email=email)
        if checked.exists():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})

def mySubscriber(request):
    return render(request, 'listSubscriber.html')

def list_subscriber(request):
    lists = SubscribeModels.objects.all().values()
    subs_list = list(lists)
    return JsonResponse(subs_list, safe=False)

def delete(request):
    if request.method == 'POST':
        email = request.POST['email']
        SubscribeModels.objects.filter(email=email).delete()
        now = list(SubscribeModels.objects.all().values())
        return JsonResponse({'deleted': True, 'now': now})

def login(request):
    if request.method == 'POST':
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(),
                                                   '608281685841-k2ub86p92rn3vmkmb13boe2ccfvll65d.apps.googleusercontent.com')
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')

            user_id = id_info['sub']
            name = id_info['name']
            email = id_info['email']
            request.session['user_id'] = user_id
            request.session['name'] = name
            request.session['email'] = email
            request.session['books'] = []

            return JsonResponse({'status': '0', 'url': reverse('main')})
        except ValueError:
            return JsonResponse({'status': '1'})
    return render(request, 'login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))




